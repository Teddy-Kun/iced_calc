#![allow(dead_code)]

use iced::widget::{button, column, text, row};
use iced::{Alignment, Element, Sandbox, Settings};

pub fn ice() -> iced::Result {
    Counter::run(Settings::default())
}

enum Operation {
    Empty,
    Add,
    Subtract,
    Multiply,
    Divide,
    Exponent,
    Root,
    SqrRoot
}

struct Counter {
    numbers: Vec<f64>,
    operations: Vec<Operation>,
    current_pos: usize,
    comma: i64,
    solve: bool
}

#[derive(Debug, Clone, Copy)]
enum Message {
    NumberPressed0,
    NumberPressed1,
    NumberPressed2,
    NumberPressed3,
    NumberPressed4,
    NumberPressed5,
    NumberPressed6,
    NumberPressed7,
    NumberPressed8,
    NumberPressed9,
    Comma,
    Solve,
    Add,
    Subtract,
    Multiply,
    Divide,
    Exponent,
    Root,
    SqrRoot
}

impl Sandbox for Counter {
    type Message = Message;

    fn new() -> Self {
        Self {
            numbers: vec![0.0],
            operations: vec![Operation::Empty],
            current_pos: 0,
            comma: 0,
            solve: false
        }
    }

    fn title(&self) -> String {
        String::from("Calculator")
    }
    
    fn update(&mut self, message: Message) {
        match message {
            Message::NumberPressed0 => {
                if self.comma != 0 {
                    println!("WIP")
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                }
            }
            Message::NumberPressed1 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.1;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 1.0;
                }
            }
            Message::NumberPressed2 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.2;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 2.0;
                }
            }
            Message::NumberPressed3 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.3;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 3.0;
                }
            }
            Message::NumberPressed4 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.4;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 4.0;
                }
            }
            Message::NumberPressed5 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.5;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 5.0;
                }
            }
            Message::NumberPressed6 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.6;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 6.0;
                }
            }
            Message::NumberPressed7 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.7;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 7.0;
                }
            }
            Message::NumberPressed8 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.8;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 8.0;
                }
            }
            Message::NumberPressed9 => {
                if self.comma != 0 {
                    self.numbers[self.current_pos] += 0.9;
                } else {
                    self.numbers[self.current_pos] *= 10.0;
                    self.numbers[self.current_pos] += 9.0;
                }
            }
            Message::Comma => {
                self.comma += 1;
            }
            Message::Add => {
                self.operations.push(Operation::Add);
            }
            Message::Subtract => {
                self.operations.push(Operation::Subtract);
            }
            Message::Multiply => {
                self.operations.push(Operation::Multiply);
            }
            Message::Divide => {
                self.operations.push(Operation::Divide);
            }
            Message::Exponent=> {
                self.operations.push(Operation::Exponent);
            }
            Message::Root => {
                self.operations.push(Operation::Root);
            }
            Message::SqrRoot => {
                self.operations.push(Operation::SqrRoot);
            }
            Message::Solve => {
                self.solve = true;
                println!("{}", parse(&self.numbers, &self.operations, &true));
            }
        }
    }

    fn view(&self) -> Element<Message> {
        column![
            text(parse(&self.numbers, &self.operations, &self.solve)).size(50),
            row![
                column![
                    row![
                        button("+").on_press(Message::Add).padding(20),
                        button("-").on_press(Message::Subtract).padding(20)
                    ].padding(20),
                    row![
                        button("*").on_press(Message::Multiply).padding(20),
                        button("/").on_press(Message::Divide).padding(20)
                    ],
                    row![
                        button("^").on_press(Message::Exponent).padding(20),
                        button("√").on_press(Message::SqrRoot).padding(20)
                    ].padding(20),
                    row![
                        button("=").on_press(Message::Solve),
                        button(",").on_press(Message::Comma)
                    ]
                ]
                .padding(20),
                column![
                    row![button("1").on_press(Message::NumberPressed1).padding(20),button("2").on_press(Message::NumberPressed2).padding(20),button("3").on_press(Message::NumberPressed3).padding(20)],
                    row![button("4").on_press(Message::NumberPressed4).padding(20),button("5").on_press(Message::NumberPressed5).padding(20),button("6").on_press(Message::NumberPressed6).padding(20)],
                    row![button("7").on_press(Message::NumberPressed7).padding(20),button("8").on_press(Message::NumberPressed8).padding(20),button("9").on_press(Message::NumberPressed9).padding(20)],
                    button("0").on_press(Message::NumberPressed0).padding(20)
                ]
                .padding(20)
            ]
            .padding(20)
            .align_items(Alignment::Center)
        ]
        .padding(20)
        .align_items(Alignment::Center)
        .into()
    }

    fn theme(&self) -> iced::Theme {
        iced::Theme::Dark
    }

    fn scale_factor(&self) -> f64 {
        1.2
    }
}

fn parse(numbers: &Vec<f64>, operations: &Vec<Operation>, solve: &bool) -> String{
    if *solve {
        println!("WIP");
    }
    
    let mut ret = String::from("");
    
    let mut pos = 0;
    for o in 0..operations.len()-1 {
        ret += &numbers[o - 1 + pos].to_string();
        
    match operations[0] {
            Operation::Add => {
                ret += " + ";
            }
            Operation::Subtract => {
                ret += " - ";
            }
            Operation::Multiply => {
                ret += " * ";
            }
            Operation::Divide => {
                ret += " / ";
            }
            Operation::Exponent => {
                ret += " ^ ";
            }
            Operation::SqrRoot => {
                ret += " √ ";
            }
            Operation::Root => {
                pos += 1;
                ret = ret + " " + &numbers[o - 1 + pos].to_string();
                ret += " √ ";
                
            }
            Operation::Empty => {}
        }
    }
    
    ret
}
